package seaassignmentfeb02_1.models;

public class ChartModel 
{
	private String title,xAxisText,yAxisText;
	private String [] categorySeries;
	private Double [] yValues;
	
	public ChartModel()
	{
		
	}	
	
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getxAxisText() {
		return xAxisText;
	}

	public void setxAxisText(String xAxisText) {
		this.xAxisText = xAxisText;
	}

	public String getyAxisText() {
		return yAxisText;
	}

	public void setyAxisText(String yAxisText) {
		this.yAxisText = yAxisText;
	}

	public String[] getCategorySeries() {
		return categorySeries;
	}

	public void setCategorySeries(String[] categorySeries) {
		this.categorySeries = categorySeries;
	}

	public double[] getyValues() {
		
		double[] yValueArrayIndouble =  new double[yValues.length];
		for(int count=0;count<yValues.length;count++)
		{
			yValueArrayIndouble[count] = yValues[count];
		}
		return yValueArrayIndouble;
	}

	public void setyValues(Double[] yValues) {
		this.yValues = yValues;
	}


}
