package seaassignmentfeb02_1.utils;

import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;

import seaassignmentfeb02_1.models.List;

public class SearchUtils 
{
	public static TableItem searchTableRow(TableViewer viewer,List list)
	{
		final Table table = viewer.getTable();
		int rowCount = table.getItemCount();
		for(int iCount=0;iCount<rowCount;iCount++)
		{
			TableItem tItem = table.getItem(iCount);
			List rowDataContent = (List)tItem.getData();
			if(rowDataContent.equals(list))
			{
				return tItem;
			}
		}
		
		return null;
	}
}
