package seaassignmentfeb02_1.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import seaassignmentfeb02_1.models.ChartModel;
import seaassignmentfeb02_1.models.InMemoryModelForViews;

/**
 * This uses an adaptor pattern
 * that transforms the model data in to a type that is understandable for the chart datasets
 * @author pranav
 *
 */
public class ChartModelAdaptor {
	
	public static ChartModel getMessageTypeDistributionChartModel()
	{
		 Map<String, Integer> messageType2Count = getMessageTypeDistribution();
		 List<String> messageTypes = new ArrayList<String>();
		 List<Double> messageCount = new ArrayList<Double>();
		 ChartModel bcModel = new ChartModel();
		 
		 
	     for (Map.Entry<String,Integer> entry : messageType2Count.entrySet()) 
	     {
	            System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());
	            messageTypes.add(entry.getKey());
	            messageCount.add(new Double(entry.getValue()));
	     } 
	     
	     bcModel.setTitle("Message Type Distribution");
	     bcModel.setxAxisText("Message Type");
	     bcModel.setyAxisText("Message Count");
	     String[] stArray = new String[messageTypes.size()]; 
	     bcModel.setCategorySeries((String[])messageTypes.toArray(stArray));
	     Double[] dbArray = new Double[messageCount.size()]; 
	     bcModel.setyValues((Double[])messageCount.toArray(dbArray));
	     return bcModel;
	}
	
	private static Map<String, Integer> getMessageTypeDistribution()
	{
		InMemoryModelForViews inMemModel = InMemoryModelForViews.getInstance();
		
		HashMap<String, Integer> messageType2Count = new HashMap<String, Integer>();
		List<seaassignmentfeb02_1.models.List> listOfRows = inMemModel.getModelRoot().getList();
		if(listOfRows==null)
		{
			return null;
		}
		for(seaassignmentfeb02_1.models.List row : listOfRows)
		{
			String messageType = row.getMsgType();
			Integer messageTypeCount = messageType2Count.get(messageType);
			if(messageTypeCount==null)
			{
				messageType2Count.put(messageType, 1);
			}
			else
			{
				messageType2Count.put(messageType, messageTypeCount+1);
			}
		}
		
		return messageType2Count;
	}
	
	public static ChartModel getReplyDistributionChartModel()
	{
		 Map<String, Integer> messageType2Count = getReplyDistribution();
		 List<String> replyType = new ArrayList<String>();
		 List<Double> replyCount = new ArrayList<Double>();
		 ChartModel bcModel = new ChartModel();
		 
		 
	     for (Map.Entry<String,Integer> entry : messageType2Count.entrySet()) 
	     {
	            System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());
	            replyType.add(entry.getKey());
	            replyCount.add(new Double(entry.getValue()));
	     } 
	     
	     bcModel.setTitle("Reply Distribution (R-Replied, N-Not replied)");
	     bcModel.setxAxisText("Reply Type");
	     bcModel.setyAxisText("Reply Type Count");
	     String[] stArray = new String[replyType.size()]; 
	     bcModel.setCategorySeries((String[])replyType.toArray(stArray));
	     Double[] dbArray = new Double[replyCount.size()]; 
	     bcModel.setyValues((Double[])replyCount.toArray(dbArray));
	     return bcModel;
	}	
	
	private static Map<String, Integer> getReplyDistribution()
	{
		InMemoryModelForViews inMemModel = InMemoryModelForViews.getInstance();
		
		HashMap<String, Integer> messageType2Count = new HashMap<String, Integer>();
		List<seaassignmentfeb02_1.models.List> listOfRows = inMemModel.getModelRoot().getList();
		if(listOfRows==null)
		{
			return null;
		}		
		for(seaassignmentfeb02_1.models.List row : listOfRows)
		{
			String messageType = row.getReplyStatus();
			Integer messageTypeCount = messageType2Count.get(messageType);
			if(messageTypeCount==null)
			{
				messageType2Count.put(messageType, 1);
			}
			else
			{
				messageType2Count.put(messageType, messageTypeCount+1);
			}
		}
		
		return messageType2Count;
	}	

}
