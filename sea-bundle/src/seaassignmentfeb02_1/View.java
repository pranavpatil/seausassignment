package seaassignmentfeb02_1;

import javax.inject.Inject;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;

import seaassignmentfeb02_1.customwidget.CustomMessageDialog;
import seaassignmentfeb02_1.exceptions.ModelInitializationException;
import seaassignmentfeb02_1.models.InMemoryModelForViews;
import seaassignmentfeb02_1.models.List;

public class View extends ViewPart {
	public static final String ID = "SEAAssignmentFeb02_1.view";

	@Inject
	IWorkbench workbench;

	private TableViewer viewer;

	private void setColorAccordingToBusinessRules(TableViewer viewer) {
		Color redColor = Display.getDefault().getSystemColor(SWT.COLOR_RED);
		Color yellowColor = Display.getDefault().getSystemColor(SWT.COLOR_YELLOW);
		Color greenColor = Display.getDefault().getSystemColor(SWT.COLOR_GREEN);
		Color blueColor = Display.getDefault().getSystemColor(SWT.COLOR_BLUE);

		final Table table = viewer.getTable();
		int tableRows = table.getItemCount();
		for (int count = 0; count < tableRows; count++) {
			TableItem tItem = table.getItem(count);
			List rowDataContent = (List) tItem.getData();
			String msgSeverityString = rowDataContent.getSeverity();
			int msgSeverity = Integer.parseInt(msgSeverityString);
			if (msgSeverity <= 50) {
				tItem.setForeground(5, yellowColor);
			} else {
				tItem.setForeground(5, redColor);
			}

			String msgReplyStatus = rowDataContent.getReplyStatus();
			if (rowDataContent.getMsgTypeText().contains("INQUIRY")) {
				if (msgReplyStatus.equals("N")) {
					tItem.setForeground(0, greenColor);
					tItem.setForeground(10, greenColor);
				} else {
					tItem.setForeground(0, blueColor);
					tItem.setForeground(10, blueColor);
				}
			}

		}

	}

	@Override
	public void createPartControl(Composite parent) {

		try {
			InMemoryModelForViews.getInstance().initialize();

			InMemoryModelForViews inMemModel = InMemoryModelForViews.getInstance();

			viewer = new TableViewer(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.BORDER);
			createColumns(parent, viewer);
			final Table table = viewer.getTable();
			table.setHeaderVisible(true);
			table.setLinesVisible(true);

			viewer.setContentProvider(new ArrayContentProvider());

			// get the content for the viewer, setInput will call getElements in the
			// contentProvider
			viewer.setInput(InMemoryModelForViews.getInstance().getModelRoot().getList());
			// make the selection available to other views
			getSite().setSelectionProvider(viewer);

			// define layout for the viewer
			GridData gridData = new GridData();
			gridData.verticalAlignment = GridData.FILL;
			gridData.horizontalSpan = 2;
			gridData.grabExcessHorizontalSpace = true;
			gridData.grabExcessVerticalSpace = true;
			gridData.horizontalAlignment = GridData.FILL;
			viewer.getControl().setLayoutData(gridData);

			setColorAccordingToBusinessRules(viewer);

			viewer.addSelectionChangedListener(new ISelectionChangedListener() {
				@Override
				public void selectionChanged(SelectionChangedEvent event) {
					IStructuredSelection selection = viewer.getStructuredSelection();
					List selectedRow = (List) selection.getFirstElement();

					Shell shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();

					MessageDialog dialog = new MessageDialog(shell, "Details", null, selectedRow.getMsgLvl02Text(),
							MessageDialog.INFORMATION, new String[] { "OK", "Reply" }, 0);

					int result = dialog.open();
					if (result == 1) {

						if (!(selectedRow.getMsgTypeText().contains("INQUIRY")
								&& selectedRow.getReplyStatus().equals("N"))) {

							MessageDialog errorMessage = new MessageDialog(shell, "Error", null,
									"Cannot reply to already replied message or a non-inquiry message",
									MessageDialog.ERROR, new String[] { "OK" }, 0);

							errorMessage.open();

						} else {

							CustomMessageDialog customDialog = new CustomMessageDialog(shell);
							customDialog.create();

							customDialog.setTitleAndMessage("Reply", "Please set the reply message");
							if (customDialog.open() == Window.OK) {
								System.out.println(customDialog.getMessage());
								selectedRow.setReplyStatus("R");
								selectedRow.setReplyText(customDialog.getMessage());
								// TableItem tItem = SearchUtils.searchTableRow(viewer, selectedRow);
								viewer.refresh();
								setColorAccordingToBusinessRules(viewer);
							}
						}
					}
				}
			});

		} catch (ModelInitializationException exp) {
			Shell shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
			MessageDialog errorMessage = new MessageDialog(shell, "Error", null, exp.getSpecificMessage(),
					MessageDialog.ERROR, new String[] { "OK" }, 0);

			errorMessage.open();
		}

	}

	@Override
	public void setFocus() {
		viewer.getControl().setFocus();
	}

	// create the columns for the table
	private void createColumns(final Composite parent, final TableViewer viewer) {
		String[] titles = { "Message Type ", "Time Stamp ", "Message Id", "msgSystem", "Severity", "Message Text",
				"Message Ref", "User", "Job", "Job No.", "Status", "Reply Text" };
		int[] bounds = { 120, 200, 100, 100, 75, 300, 300, 100, 130, 100, 50, 300 };

		TableViewerColumn col = createTableViewerColumn(titles[0], bounds[0], 0,
				"Indicates the message type. Only Inquiry messages could be replied to");
		col.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				seaassignmentfeb02_1.models.List list = (seaassignmentfeb02_1.models.List) element;
				return list.getMsgTypeText();
			}
		});

		col = createTableViewerColumn(titles[1], bounds[1], 1, "");
		col.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				seaassignmentfeb02_1.models.List list = (seaassignmentfeb02_1.models.List) element;
				return list.getTimeStamp();
			}
		});

		col = createTableViewerColumn(titles[2], bounds[2], 2, "");
		col.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				seaassignmentfeb02_1.models.List list = (seaassignmentfeb02_1.models.List) element;
				return list.getMsgId();
			}
		});

		col = createTableViewerColumn(titles[3], bounds[3], 3, "");
		col.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				seaassignmentfeb02_1.models.List list = (seaassignmentfeb02_1.models.List) element;
				return list.getMsgSystem();
			}
		});

		col = createTableViewerColumn(titles[4], bounds[4], 4, "");
		col.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				seaassignmentfeb02_1.models.List list = (seaassignmentfeb02_1.models.List) element;
				return list.getSeverity();
			}
		});

		col = createTableViewerColumn(titles[5], bounds[5], 5, "");
		col.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				seaassignmentfeb02_1.models.List list = (seaassignmentfeb02_1.models.List) element;
				return list.getMsgText();
			}
		});

		col = createTableViewerColumn(titles[6], bounds[6], 6, "");
		col.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				seaassignmentfeb02_1.models.List list = (seaassignmentfeb02_1.models.List) element;
				return list.getMsgRef();
			}
		});

		col = createTableViewerColumn(titles[7], bounds[7], 7, "");
		col.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				seaassignmentfeb02_1.models.List list = (seaassignmentfeb02_1.models.List) element;
				return list.getUser();
			}
		});

		col = createTableViewerColumn(titles[8], bounds[8], 8, "");
		col.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				seaassignmentfeb02_1.models.List list = (seaassignmentfeb02_1.models.List) element;
				return list.getJob();
			}
		});

		col = createTableViewerColumn(titles[9], bounds[9], 9, "");
		col.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				seaassignmentfeb02_1.models.List list = (seaassignmentfeb02_1.models.List) element;
				return list.getJobNbr();
			}
		});

		col = createTableViewerColumn(titles[10], bounds[10], 10, "N indicates not replied. R indicates replied");
		col.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				seaassignmentfeb02_1.models.List list = (seaassignmentfeb02_1.models.List) element;
				return list.getReplyStatus();
			}
		});

		col = createTableViewerColumn(titles[11], bounds[11], 11, "");
		col.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				seaassignmentfeb02_1.models.List list = (seaassignmentfeb02_1.models.List) element;
				return list.getReplyText();
			}
		});

	}

	private TableViewerColumn createTableViewerColumn(String title, int bound, final int colNumber,
			String toolTipText) {
		final TableViewerColumn viewerColumn = new TableViewerColumn(viewer, SWT.NONE);
		final TableColumn column = viewerColumn.getColumn();
		column.setText(title);
		column.setWidth(bound);
		column.setResizable(true);
		column.setMoveable(true);
		column.setToolTipText(toolTipText);
		return viewerColumn;
	}
}