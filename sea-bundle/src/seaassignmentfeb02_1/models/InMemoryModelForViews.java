package seaassignmentfeb02_1.models;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.google.gson.Gson;

import seaassignmentfeb02_1.exceptions.ModelInitializationException;
import seaassignmentfeb02_1.listeners.PropertyChangeListener;

/**
 * This is a model where the file contents that are read are kept and read Based
 * on the singleton pattern
 * 
 * @author pranav
 *
 */
public class InMemoryModelForViews 
{
	private static InMemoryModelForViews inMemoryModelForViewsInstance = null;
	private Gson gson = new Gson();
	private ModelRoot modelRoot = null;
	private static final Logger logger = Logger.getLogger(InMemoryModelForViews.class);
	private List<PropertyChangeListener> listenerList = new ArrayList<PropertyChangeListener>();
	

	private InMemoryModelForViews() 
	{

	}

	public static InMemoryModelForViews getInstance() 
	{
		if (inMemoryModelForViewsInstance == null) 
		{
			inMemoryModelForViewsInstance = new InMemoryModelForViews();
		}
		
		return inMemoryModelForViewsInstance;
	}
	
	public void initialize() throws ModelInitializationException
	{
		
		if(inMemoryModelForViewsInstance.getModelRoot()!=null)
		{
			return;
		}
		
		String parseFileLoc = System.getProperty("parsefileloc");
		
		if(parseFileLoc==null)
		{
			throw new ModelInitializationException("Property parsefileloc not set- Kindly set the property for file reading");
		}
		
		BufferedReader bufReader;
		try 
		{
			bufReader = new BufferedReader(new FileReader(parseFileLoc));
			modelRoot = gson.fromJson(bufReader, ModelRoot.class);
			logger.debug("File at " + parseFileLoc + " read successfully");
		} 
		catch (FileNotFoundException fnfExc) 
		{
			fnfExc.printStackTrace();
			logger.error("Error reading file at : " + parseFileLoc);
			logger.error("Exception : " + fnfExc);
			throw new ModelInitializationException("Error reading file at : " + parseFileLoc);			
		}
	}
	
	public ModelRoot getModelRoot()
	{
		return modelRoot;
	}
	
	public void addPropertyChangeListener(PropertyChangeListener pcListener)
	{
		listenerList.add(pcListener);
	}
	
	/**
	 * Gets triggered when any property of the model is changed
	 * @param value
	 */
	public void firePropertyChanged(Object value)
	{
		for(PropertyChangeListener listener: listenerList)
		{
			listener.valueChanged(value);
		}
	}
	
	
}
