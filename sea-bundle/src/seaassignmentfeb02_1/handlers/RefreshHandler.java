package seaassignmentfeb02_1.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.handlers.HandlerUtil;
import seaassignmentfeb02_1.models.InMemoryModelForViews;
import seaassignmentfeb02_1.views.DashboardView;

public class RefreshHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		
		try 
		{
			HandlerUtil.getActiveWorkbenchWindow(event).getActivePage().showView(seaassignmentfeb02_1.View.ID);
			HandlerUtil.getActiveWorkbenchWindow(event).getActivePage().showView(DashboardView.ID);
			/**
			 * Ideally this should get triggered when the model changes
			 * But for simplicity I have used an observer to re-render the graphs
			 */
			InMemoryModelForViews.getInstance().firePropertyChanged(null);
			
		} catch (PartInitException e) {

			e.printStackTrace();
		}
		return null;
	}
}
